FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.19.1

COPY "cargo-nextest-bin" "/usr/local/bin/cargo-nextest"
